from selenium import webdriver

from selenium.webdriver.common.keys import Keys

import os

import time

#open browser chrome
driver = webdriver.Chrome()

#open bukalapak
driver.get('https://www.bukalapak.com') 
time.sleep(4)

input_login = driver.find_element_by_id('login_link') 
input_login.send_keys(Keys.RETURN)
time.sleep(3)

input_user = driver.find_element_by_xpath('//*[@id="user_session_username"]') 
input_user.send_keys("")
time.sleep(1)

input_pass = driver.find_element_by_xpath('//*[@id="user_session_password"]') 
input_pass.send_keys("")
time.sleep(1)

input_pass.send_keys(Keys.RETURN)
time.sleep(3)

#send sms
driver.find_element_by_xpath('//*[@id="popup_wrapper"]/div/div/div[2]/form/div[4]/div[3]/div/div[1]/span').click()
time.sleep(3)
txt = driver.find_element_by_xpath('//*[@id="otp"]')
txt.send_keys("")
time.sleep(9)
txt.send_keys(Keys.RETURN)
time.sleep(5)

driver.find_element_by_xpath('//*[@id="popup_wrapper"]/div/div/div[2]/form/div[4]/div[3]/div/div[3]').click()
time.sleep(3)

#search data
input_search = driver.find_element_by_xpath('//*[@id="v-omnisearch__input"]') 
input_search.send_keys("oppo f3")
input_search.send_keys(Keys.RETURN)
time.sleep(3)

#checout barang
driver.find_element_by_xpath('//*[@id="reskinned_page"]/div[6]/section[2]/div/div/section[2]/ul/li[1]/div[1]/div[2]/div[1]/a').click()
time.sleep(3)
checkout = driver.find_element_by_xpath('//*[@id="mod-product-detail-1"]/div[1]/div[2]/div/div/div[2]/div[6]/form[1]/div[2]/div/button')
checkout.click()
time.sleep(3)
